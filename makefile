############################# Makefile ##########################
.PHONY: all clean cleanall

all: dla.exe

dla.exe: main.o dla.o bmpfile.o
	gcc main.o dla.o bmpfile.o -o dla.exe -std=c99
	
main.o: main.c
	gcc -c main.c -o main.o -W -Wall -pedantic -std=c99

dla.o: dla.c
	gcc -c dla.c -o dla.o -W -Wall -pedantic -std=c99

bmpfile.o: libbmp/bmpfile.c
	gcc -c libbmp/bmpfile.c -o bmpfile.o -W -Wall -pedantic -std=c99

clean:
	rm -rf *.o

cleanall:
	rm -rf *.o *.exe *.bmp *.txt