/*************************************************************************************
* Includes
*************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "DLA.h"

/*************************************************************************************
* Defines
*************************************************************************************/
/* Tamanho padrão da matriz */
#define L_SIZE 				(500)
#define C_SIZE 				(500)

/* Numero de elementos */
#define ELEMENTS 			(10000)

/* Máximo de interações permitido para obter local não ocupado */
#define MAX_INTERACTIONS 	(50)

/* Nome dos arquivos */
#define FILENAME_IN_IMAGE 	"in.bmp"
#define FILENAME_OUT_IMAGE	"out.bmp"

/*******************************************************************************
   main
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
int main(int argc, char* argv[])
{
	/* Estrutura de controle */
	DLA_control_t control = {
		.matrix 	= NULL,
		.lSize 		= L_SIZE,
		.cSize		= C_SIZE,
		.elements	= ELEMENTS,
		.maxInteractions = MAX_INTERACTIONS,
	};

	/* Obtém configurações */
	if(argc == 2)
		control.elements = (uint32_t) atoi(argv[1]);
	else if(argc > 2)
	{
		control.elements = (uint32_t) atoi(argv[1]);
		control.maxInteractions = (uint32_t) atoi(argv[2]);
	}

	/* Verifica se conseguiu carregar imagem */
	if(DLA_imageLoader(&control, FILENAME_IN_IMAGE) != 0)
	{
		printf("WARNING: Arquivo de imagem \"%s\" não pôde ser acessado. Gerando matriz inicial padrão.\n", FILENAME_IN_IMAGE);

		/* Gera matriz padrão inicial */
		control.matrix = malloc(control.lSize * control.cSize);
		if(!control.matrix)
		{
			printf("ERROR: Não foi possivel alocar matriz inicial.\n");
			exit(-1);
		}
		memset(control.matrix, 0, control.lSize * control.cSize);
	}
	else
		printf("PROGRESS: Arquivo de imagem \"%s\" carregado com sucesso.\n", FILENAME_IN_IMAGE);

    /* Inicializa busca por solução */
	printf("PROGRESS: Iniciando solução.\n");
    if(!DLA_init(&control))
    {
		free(control.matrix);
        printf("ERROR: Número máximo de interações atingido.\n");
        exit(-1);
    }
	
	/* Cria uma imagem do tipo bitmap a partir da matriz */
	if(DLA_imageCreator(&control, FILENAME_OUT_IMAGE) != 0)
	{
		free(control.matrix);
		printf("ERROR: Não foi possível salvar arquivo de imagem final.\n");
        exit(-1);
	}
	
	free(control.matrix);
	printf("SUCCESS: Arquivo de imagem \"%s\" gerado com sucesso.\n", FILENAME_OUT_IMAGE);
	return 0;
}