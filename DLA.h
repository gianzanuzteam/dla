#ifndef _DLA_H_
#define _DLA_H_

#ifdef __cplusplus
extern "C" {
#endif

/*************************************************************************************
* Includes
*************************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*************************************************************************************
* Defines
*************************************************************************************/
/* Imprime na tela o progresso de conclusão */
//#define PRINT_PROGRESS
/* Imprime na tela o progresso da matrix */
//#define PRINT_MATRIX
/* Habilita procurar solução nas diagonais do elemento */
#define DIAGONAL_ENABLE

#define DLA_BACKGROUND_COLOUR_R		(0)
#define DLA_BACKGROUND_COLOUR_G		(0)
#define DLA_BACKGROUND_COLOUR_B		(0)

#define DLA_MAIN_COLOUR_R			(255)
#define DLA_MAIN_COLOUR_G			(255)
#define DLA_MAIN_COLOUR_B			(255)

typedef struct DLA_control_tag {
	uint32_t elements;
	uint32_t maxInteractions;
	/* ... */
	uint32_t lSize;
	uint32_t cSize;
	/* ... */
	uint8_t* matrix;
} DLA_control_t;

/*************************************************************************************
* Public prototypes
*************************************************************************************/
/* Inicializa ferramenta DLA */
bool DLA_init(DLA_control_t* control);

/* 	Lê o arquivo que contém a posição inicial do aglomerado, dado pelo descritor 'fd',
	e salva na matriz 'matrix', dada pelo ponteiro da primeira posição */
int DLA_readMatrix(DLA_control_t* control, FILE *fd);

/* 	Escreve a matriz dada pelo ponteiro da primeira posição, no descritor de arquivo 'fd'.
	É necessário indicar o tamanho das linhas e colunas da matriz, atráves das variáveis 'lSize' e 'cSize' */
int DLA_writeMatrix(DLA_control_t* control, FILE *fd);

/* 	Imprime na tela uma matriz dada pelo ponteiro da primeira posição.
	É necessário indicar o tamanho das linhas e colunas da matriz, atráves das variáveis 'lSize' e 'cSize' */
int DLA_printMatrix(DLA_control_t* control);

/* 	Salva matriz de cores em uma imagem no formato bitmap */
int DLA_imageCreator(DLA_control_t* control, const char* filename);

/* 	Salva matriz de cores em uma imagem no formato bitmap, em buffer */
int DLA_imageCreator_buffer(DLA_control_t* control, uint8_t** buffer, uint32_t* size);

/* 	Carrega matriz de cores de uma imagem no formato bitmap */
int DLA_imageLoader(DLA_control_t* control, const char* filename);

#ifdef __cplusplus
}
#endif

#endif /* _DLA_H_ */