/*************************************************************************************
* Includes
*************************************************************************************/
#include "DLA.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "libbmp/bmpfile.h"

/*************************************************************************************
* Private prototypes
*************************************************************************************/
/* 	Verifica se uma determinada posição da matriz está ocupada por conglomerados vizinhos.
	É necessário indicar o tamanho das linhas e colunas da matriz, atráves das variáveis 'lSize' e 'cSize' */
bool DLA_matrixBusy(DLA_control_t* control, uint32_t lElement, uint32_t cElement);

/*******************************************************************************
   matrixBusy
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
bool DLA_init(DLA_control_t* control)
{
	register uint32_t line, column;

	/* Inicializa a semente randômica, utilizando os segundos do horário da máquina */
	srand(time(NULL));
	
	/* Interação para geração de conglomerados */
	for(uint32_t i=0; i<control->elements; i++)
	{
		uint32_t count = 0;
		/* Inicialmente procura uma posição na matriz que esteja livre */
		do
		{
			line = rand() % control->lSize;
			column = rand() % control->cSize;
			
			/* Verifica numero máximo de interações */
			count++;
			if(count > control->maxInteractions)
				return false;
				
		} while(DLA_matrixBusy(control, line, column));
		
		/* Após, o conglomerado segue um caminho aleatório até encontrar um vizinho */
		while(!DLA_matrixBusy(control, line, column))
		{
#ifdef DIAGONAL_ENABLE
			/* Gera um número randômico de 0 à 7... */
			uint8_t random = rand() % 8;
#else
			/* Gera um número randômico de 0 à 3... */
			uint8_t random = rand() % 4;
#endif
			switch(random){
				/* Para baixo */
				case 0:
					line -= 1;
					break;
				/* Para cima */
				case 1:
					line += 1;
					break;
				/* Para esquerda */
				case 2:
					column -= 1;
					break;
				/* Para direita */
				case 3:
					column += 1;
					break;
#ifdef DIAGONAL_ENABLE
				/* Para diagonal superior esquerda */
				case 4:
					line -= 1;
					column -= 1;
					break;
				/* Para diagonal superior direita */
				case 5:
					line -= 1;
					column += 1;
					break;
				/* Para diagonal inferior esquerda */
				case 6:
					line += 1;
					column -= 1;
					break;
				/* Para diagonal inferior direita */
				case 7:
					line += 1;
					column += 1;
					break;
#endif
				}
		}
		/* Coloca o conglomerado na matriz, indicando a posição final */
		control->matrix[control->cSize*line + column] = 1;
		
#ifdef PRINT_MATRIX
		/* Imprime a matriz inicial de conglomerados na tela */
		DLA_printMatrix(control);
		printf("\n\n");
#endif
#ifdef PRINT_PROGRESS
		/* Print na tela da porcentagem */
		printf("\n%d%%",100*i/(elements-1));
#endif
	}

	return true;
}

/*******************************************************************************
   matrixBusy
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
bool DLA_matrixBusy(DLA_control_t* control, uint32_t lElement, uint32_t cElement)
{
	/* Verifica a posição do elemento */
	if(control->matrix[control->cSize*lElement + cElement])
		return true;
	
	/* Verifica as bordas superiores e inferiores (extremidades da matriz) */
	if(lElement < 1 || lElement > control->lSize-2)
		return true;
	
	/* Verifica as bordas laterais (extremidades da matriz) */
	if(cElement < 1 || cElement > control->cSize-2)
		return true;
	
	/* Verifica posição acima e abaixo do elemento */
	if(control->matrix[control->cSize*(lElement-1) + cElement] || control->matrix[control->cSize*(lElement+1) + cElement])
		return true;
	
	/* Verifica posição na esquerda e na direita do elemento */
	if(control->matrix[control->cSize*(lElement) + (cElement-1)] || control->matrix[control->cSize*(lElement) + (cElement+1)])
		return true;
	
#ifdef DIAGONAL_ENABLE
	/* Verifica posição diagonais superiores */
	if(control->matrix[control->cSize*(lElement-1) + (cElement-1)] || control->matrix[control->cSize*(lElement-1) + (cElement+1)])
		return true;

	/* Verifica posição diagonais inferiores */
	if(control->matrix[control->cSize*(lElement+1) + (cElement-1)] || control->matrix[control->cSize*(lElement+1) + (cElement+1)])
		return true;
#endif

	/* Vizinhança livre */
	return false;
}

/*******************************************************************************
   DLA_readMatrix
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
int DLA_readMatrix(DLA_control_t* control, FILE *fd)
{
	uint32_t countLine = 0;
	
	/* Continuar a ler até o fim do arquivo */
	while(!feof(fd))
	{
		/* Verificação de linhas na matriz */
		countLine++;
		
		/* Encerra caso matriz inicial possui tamanho incompativel com o declarado */
		if(countLine > control->lSize + 10)
		{
			printf("ERROR: Matriz inicial com tamanho incorreto de linhas.\n");
			exit(1);
		}

		/* 	A variável 'lineMatrix' contém uma linha do arquivo que
			indica a posição inicial do aglomerado */
		char *lineMatrix = calloc(control->cSize + 10, sizeof(char));
		
		/* 	Verifica espaço alocado */
		if(!lineMatrix)
		{
			printf("ERROR: Não foi possível alocar matriz inicial.\n");
			exit(1);
		}
		
		/* Lê uma linha, e procura pelos simbolos 'O' e '-' */
		memset(lineMatrix, 0, control->cSize + 10);
		fgets(lineMatrix, control->cSize + 10, fd);
		
		char *strtoken = lineMatrix;
		uint32_t countColumm = 0;
		while(*strtoken != '\n' && *strtoken != '\0')
		{
			if(*strtoken == 'O')
				*control->matrix = 1;
			else if(*strtoken == '-')
				*control->matrix = 0;
			else
				printf("WARNING: Ignorando caractére inválido na linha %d.\n",countLine);
			strtoken++;
			control->matrix++;
			
			/* Verificação de colunas na matriz */
			countColumm++;
			if(countColumm > control->cSize)
			{
				printf("ERRO: Matriz inicial com tamanho incorreto de colunas. Erro na linha %d.\n", countLine);
				exit(1);
			}
		}

		/* Libera espaço previamente ocupado */
		free(lineMatrix);
	}
	
	/* Encerra caso matriz inicial possui tamanho incompativel com o declarado */
	if(countLine < control->lSize)
	{
		printf("ERRO: Matriz inicial com tamanho incorreto de linhas.\n");
		exit(1);
	}
	
	fclose(fd);
	return 0;
}

/*******************************************************************************
   DLA_writeMatrix
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
int DLA_writeMatrix(DLA_control_t* control, FILE *fd)
{
	uint32_t l, c;
	/* Para cada linha... */
	for (l=0; l<control->lSize; ++l)
	{
		/* E para cada coluna */
		for (c=0; c<control->cSize; ++c)
		{
			/* Se possuir um '1', então escreve um aglomerado dado por 'O' */
			if(*control->matrix)
				putc('O',fd);
			/* Ou caso contrário, deixa o espaço como '-' */
			else
				putc('-',fd);
			/* Avança o ponteiro da matriz, uma vez que é armazenada sequencialmente */
			control->matrix++;
		}
		putc('\n',fd);
	}
	fclose(fd);
	return 0;
}

/*******************************************************************************
   DLA_printMatrix
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
int DLA_printMatrix(DLA_control_t* control)
{
	uint32_t l, c;
	for (l=0; l<control->lSize; ++l)
	{
		for (c=0; c<control->cSize; ++c)
		{
			printf("%d ", *control->matrix);
			/* Avança o ponteiro da matriz, uma vez que é armazenada sequencialmente */
			control->matrix++;
		}
		printf("\n");
	}
	return 0;
}

/*******************************************************************************
   DLA_imageCreator
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
int DLA_imageCreator(DLA_control_t* control, const char* filename)
{
	/* Cria arquivo .bmp */
	bmpfile_t* bmpFile = bmp_create(control->cSize, control->lSize, 24);
	if(!bmpFile)
		return -1;

	/* Preenche pixels da imagem */
	for(uint32_t y=0; y<control->lSize; y++)
	{
		for(uint32_t x=0; x<control->cSize; x++)
		{
			rgb_pixel_t pixel;
			pixel.red 	= (control->matrix[x + y*control->cSize]) ? DLA_MAIN_COLOUR_R : DLA_BACKGROUND_COLOUR_R;
			pixel.green = (control->matrix[x + y*control->cSize]) ? DLA_MAIN_COLOUR_G : DLA_BACKGROUND_COLOUR_G;
			pixel.blue 	= (control->matrix[x + y*control->cSize]) ? DLA_MAIN_COLOUR_B : DLA_BACKGROUND_COLOUR_B;
			pixel.alpha = 0;
			bmp_set_pixel(bmpFile, x, y, pixel); 
		}
	}

	/* Salva no arquivo */
	bmp_save(bmpFile, filename);
	
	/* Destroi .bmp */
	bmp_destroy(bmpFile);
	
	return 0;
}

/*******************************************************************************
   DLA_imageCreator_buffer
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
int DLA_imageCreator_buffer(DLA_control_t* control, uint8_t** buffer, uint32_t* size)
{
	/* Cria arquivo .bmp */
	bmpfile_t* bmpFile = bmp_create(control->cSize, control->lSize, 24);
	if(!bmpFile)
		return -1;

	/* Preenche pixels da imagem */
	for(uint32_t y=0; y<control->lSize; y++)
	{
		for(uint32_t x=0; x<control->cSize; x++)
		{
			rgb_pixel_t pixel;
			pixel.red 	= (control->matrix[x + y*control->cSize]) ? DLA_MAIN_COLOUR_R : DLA_BACKGROUND_COLOUR_R;
			pixel.green = (control->matrix[x + y*control->cSize]) ? DLA_MAIN_COLOUR_G : DLA_BACKGROUND_COLOUR_G;
			pixel.blue 	= (control->matrix[x + y*control->cSize]) ? DLA_MAIN_COLOUR_B : DLA_BACKGROUND_COLOUR_B;
			pixel.alpha = 0;
			bmp_set_pixel(bmpFile, x, y, pixel); 
		}
	}

	/* Salva no buffer */
	bmp_save_buffer(bmpFile, buffer, size);
	
	/* Destroi .bmp */
	bmp_destroy(bmpFile);
	
	return 0;
}

/*******************************************************************************
   DLA_imageLoader
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
int DLA_imageLoader(DLA_control_t* control, const char* filename)
{
	/* Cria arquivo .bmp do arquivo */
	bmpfile_t* bmpFile = bmp_create_from_file(filename);
	if(!bmpFile)
		return -1;

	/* Aloca memória para matriz */
	control->lSize = bmp_get_height(bmpFile);
	control->cSize = bmp_get_width(bmpFile);
	control->matrix = (uint8_t*) malloc((control->lSize) * (control->cSize));
	if(!control->matrix)
	{
		bmp_destroy(bmpFile);
		return -1;
	}

	/* Preenche pixels da imagem */
	for(uint32_t y=0; y<(control->lSize); y++)
	{
		for(uint32_t x=0; x<(control->cSize); x++)
		{
			rgb_pixel_t* pixel = bmp_get_pixel(bmpFile, x, y);
			control->matrix[x + y*(control->cSize)] = (uint8_t) (pixel->red | pixel->green | pixel->blue);
		}
	}

	/* Destroi .bmp */
	bmp_destroy(bmpFile);

	return 0;
}